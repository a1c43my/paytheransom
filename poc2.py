import os,time,socket,pyAesCrypt

size = 128 * 1024
password = "password"
def victimize():
   for root, dirs, files in os.walk("level2", topdown=True):
      for name in files:
        cur = str(os.path.join(root, name))
        pyAesCrypt.encryptFile(f"{cur}", f"{cur}.aes", passw="level1pass", bufferSize=size)
        time.sleep(1)
        print(f"you just lost {cur})")
   with open("level1/hint.txt","w") as runr:
      runr.write("the number is always 3")
      runr.close()
   
         

def recover():
   for root, dirs, files in os.walk("level2", topdown=True):
      for name in files:
         cur = str(os.path.join(root, name))
         time.sleep(1)
         print(f"you just FOUND {cur})")
         #fail safe for practice
         try:
            pyAesCrypt.decryptFile(f"{cur}.aes", f"{cur}.backup.copy.txt", passw="level1pass", bufferSize=size)
         except ValueError:
            pass

victimize()
revive = 0
while True:
   revive = input('guess me !!!')
   if int(revive) == 3:
      print('you guessed '+revive)
      recover()
      print('good job')
      exit()

